libserial (1.0.0-7) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload

  [ Helmut Grohne ]
  * Non-maintainer upload.
  * Fix duplicate override_dh_auto_configure. (Closes: #985608)

 -- Helmut Grohne <helmut@subdivi.de>  Fri, 19 Mar 2021 18:37:14 +0100

libserial (1.0.0-6) unstable; urgency=medium

  [ Helmut Grohne ]
  * Drop unused python3-sip-dev dependency. (Closes: #984812)
  * Further reduce Build-Depends via <!nocheck> + --disable-tests. (Closes: #984611)

  [ Gianfranco Costamagna ]
  * Bump compat level to 13
  * Add .a and .la file to not-installed

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 18 Mar 2021 14:38:41 +0100

libserial (1.0.0-5) unstable; urgency=medium

  [ Helmut Grohne ]
  * Reduce Build-Depends: (Closes: #982214)
    + Drop unused docbook, doxygen, graphviz, and intltool.
    + Demote sphinx dependencies to B-D-I.

  [ Gianfranco Costamagna ]
  * Upload to sid

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 08 Feb 2021 21:30:32 +0100

libserial (1.0.0-4) unstable; urgency=medium

  * Don't depend on boost anymore
  * Fix doc package

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 11 Mar 2020 09:42:35 +0100

libserial (1.0.0-3) unstable; urgency=medium

  * debian/patches/818005ee7d19b9145bc875cb0ea10422e6e0afaf.patch:
    - upstream patch to fix build on some 32bit systems.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 10 Mar 2020 22:30:02 +0100

libserial (1.0.0-2) unstable; urgency=medium

  * Add patch to fix build on some systems with picky flags

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 17 Feb 2020 15:14:20 +0100

libserial (1.0.0-1) unstable; urgency=medium

  * Update watch file for github move
  * New upstream version 1.0.0
  * update watch file to v4
  * Bump copyright years for Debian packaging
  * Move license to BSD-3-clause, upstream moved it
    and the Debian packaging has been fully done by me
  * Update copyright years
  * Move homepage to github location
  * Bump std-version to 4.5.0
  * Update VCS fields for salsa.d.o move
  * Bump compat level to 12
  * Drop default autoreconf
  * Switch python-sip to python3-sip (Closes: #943101)
    - port to Python3
  * Add libgtest dependency, used for testing
  * Add sphinx and rtd-theme dependencies, used in html generation
  * Switch library SONAME to 1

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 17 Feb 2020 12:34:45 +0100

libserial (0.6.0~rc2+svn122-4) unstable; urgency=medium

  * Make it build with -A and -B to help Debian buildds.
    - run tests only in -B mode.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 14 Sep 2015 08:34:07 +0200

libserial (0.6.0~rc2+svn122-3) unstable; urgency=medium

  * Upload with the all package, it fails to build in source-only mode.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 11 Sep 2015 19:05:48 +0200

libserial (0.6.0~rc2+svn122-2) unstable; urgency=medium

  * Update my uid.
  * Fix typo in package description.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 11 Sep 2015 18:13:25 +0200

libserial (0.6.0~rc2+svn122-1) unstable; urgency=medium

  * New upstream snapshot.
    - drop all patches, accepted upstream.
  * Add graphviz as b-d, helping with doc generation.
    - remove .md5 and .map files.
  * Fix copyright file, the correct license is GPL-2+,
    a *great* thanks goes to Thorsten Alteholz, for the
    help!

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Mon, 20 Apr 2015 18:49:54 +0200

libserial (0.6.0~rc1+svn114-1) unstable; urgency=medium

  * Initial release (Closes: #780269).

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Wed, 11 Mar 2015 10:21:59 +0100
